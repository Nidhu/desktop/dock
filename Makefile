SRC = ./src
BUILD = ./build
TARGET = /usr/bin/ndh-dock

prepare: 
	mkdir -p $(BUILD)

build: prepare
	cd $(BUILD) && qmake .$(SRC) && make
	mv $(BUILD)/src $(BUILD)/ndh-dock

install: 
	mv $(BUILD)/ndh-dock $(TARGET)
	make clean

clean:
	cd $(BUILD) && make clean
	rm -rf $(BUILD)*
