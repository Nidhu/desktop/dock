#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  MainWindow(QWidget *parent = nullptr);
  ~MainWindow();

  char **read(char *desktopFile);
  void addWidgets(char *desktopFiles);
};
#endif // MAINWINDOW_H
