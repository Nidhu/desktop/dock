#include "mainwindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
  QApplication a(argc, argv);
  MainWindow w;
  w.addWidgets("~/.config/nidhu/dock/");
  w.show();
  return a.exec();
}
